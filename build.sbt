name := "dependent-types"

version := "0.1"

scalaVersion := "2.13.6"

libraryDependencies ++= Seq(
  "eu.timepit" %% "refined"                 % "0.9.27"
)