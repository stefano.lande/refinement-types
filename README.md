# Refinement types

## Examples

- UnsafeMain: no input validation
- SafeMain: manual input validation - runtime errors
- TypedMain: validation with refinement types - compile time errors
- RuntimeValues: validation with refinement types - runtime errors