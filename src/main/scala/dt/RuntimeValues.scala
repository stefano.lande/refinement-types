package dt

import dt.RuntimeValues.Splitter.split
import eu.timepit.refined._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric._
import eu.timepit.refined.auto._

import scala.io.StdIn

object RuntimeValues extends App {

  val chunkSize = StdIn.readInt()
  val list      = List(1, 2, 3, 4, 5, 6, 7)

  refineV[Positive](chunkSize) map { refinedChunkSize =>
    val splitList = split(list, refinedChunkSize)
    println(splitList)
  }

  object Splitter {

    def split[A](list: List[A], chunkSize: Int Refined Positive): List[List[A]] =
      if (list.isEmpty) Nil
      else (list take chunkSize) :: split(list drop chunkSize, chunkSize)
  }

}
