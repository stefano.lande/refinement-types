## Provided predicates

The library comes with these predefined predicates:

[`boolean`](https://github.com/fthomas/refined/blob/master/modules/core/shared/src/main/scala-3.0+/eu/timepit/refined/boolean.scala)

* `True`: constant predicate that is always `true`
* `False`: constant predicate that is always `false`
* `Not[P]`: negation of the predicate `P`
* `And[A, B]`: conjunction of the predicates `A` and `B`
* `Or[A, B]`: disjunction of the predicates `A` and `B`
* `Xor[A, B]`: exclusive disjunction of the predicates `A` and `B`
* `Nand[A, B]`: negated conjunction of the predicates `A` and `B`
* `Nor[A, B]`: negated disjunction of the predicates `A` and `B`
* `AllOf[PS]`: conjunction of all predicates in `PS`
* `AnyOf[PS]`: disjunction of all predicates in `PS`
* `OneOf[PS]`: exclusive disjunction of all predicates in `PS`

[`char`](https://github.com/fthomas/refined/blob/master/modules/core/shared/src/main/scala/eu/timepit/refined/char.scala)

* `Digit`: checks if a `Char` is a digit
* `Letter`: checks if a `Char` is a letter
* `LetterOrDigit`: checks if a `Char` is a letter or digit
* `LowerCase`: checks if a `Char` is a lower case character
* `UpperCase`: checks if a `Char` is an upper case character
* `Whitespace`: checks if a `Char` is white space

[`collection`](https://github.com/fthomas/refined/blob/master/modules/core/shared/src/main/scala-3.0+/eu/timepit/refined/collection.scala)

* `Contains[U]`: checks if an `Iterable` contains a value equal to `U`
* `Count[PA, PC]`: counts the number of elements in an `Iterable` which satisfy the
  predicate `PA` and passes the result to the predicate `PC`
* `Empty`: checks if an `Iterable` is empty
* `NonEmpty`: checks if an `Iterable` is not empty
* `Forall[P]`: checks if the predicate `P` holds for all elements of an `Iterable`
* `Exists[P]`: checks if the predicate `P` holds for some elements of an `Iterable`
* `Head[P]`: checks if the predicate `P` holds for the first element of an `Iterable`
* `Index[N, P]`: checks if the predicate `P` holds for the element at index `N` of a sequence
* `Init[P]`: checks if the predicate `P` holds for all but the last element of an `Iterable`
* `Last[P]`: checks if the predicate `P` holds for the last element of an `Iterable`
* `Tail[P]`: checks if the predicate `P` holds for all but the first element of an `Iterable`
* `Size[P]`: checks if the size of an `Iterable` satisfies the predicate `P`
* `MinSize[N]`: checks if the size of an `Iterable` is greater than or equal to `N`
* `MaxSize[N]`: checks if the size of an `Iterable` is less than or equal to `N`

[`generic`](https://github.com/fthomas/refined/blob/master/modules/core/shared/src/main/scala-3.0+/eu/timepit/refined/generic.scala)

* `Equal[U]`: checks if a value is equal to `U`

[`numeric`](https://github.com/fthomas/refined/blob/master/modules/core/shared/src/main/scala-3.0+/eu/timepit/refined/numeric.scala)

* `Less[N]`: checks if a numeric value is less than `N`
* `LessEqual[N]`: checks if a numeric value is less than or equal to `N`
* `Greater[N]`: checks if a numeric value is greater than `N`
* `GreaterEqual[N]`: checks if a numeric value is greater than or equal to `N`
* `Positive`: checks if a numeric value is greater than zero
* `NonPositive`: checks if a numeric value is zero or negative
* `Negative`: checks if a numeric value is less than zero
* `NonNegative`: checks if a numeric value is zero or positive
* `Interval.Open[L, H]`: checks if a numeric value is in the interval (`L`, `H`)
* `Interval.OpenClosed[L, H]`: checks if a numeric value is in the interval (`L`, `H`]
* `Interval.ClosedOpen[L, H]`: checks if a numeric value is in the interval [`L`, `H`)
* `Interval.Closed[L, H]`: checks if a numeric value is in the interval [`L`, `H`]
* `Modulo[N, O]`: checks if an integral value modulo `N` is `O`
* `Divisible[N]`: checks if an integral value is evenly divisible by `N`
* `NonDivisible[N]`: checks if an integral value is not evenly divisible by `N`
* `Even`: checks if an integral value is evenly divisible by 2
* `Odd`: checks if an integral value is not evenly divisible by 2
* `NonNaN`: checks if a floating-point number is not NaN

[`string`](https://github.com/fthomas/refined/blob/master/modules/core/shared/src/main/scala-3.0+/eu/timepit/refined/string.scala)

* `EndsWith[S]`: checks if a `String` ends with the suffix `S`
* `IPv4`: checks if a `String` is a valid IPv4
* `IPv6`: checks if a `String` is a valid IPv6
* `MatchesRegex[S]`: checks if a `String` matches the regular expression `S`
* `Regex`: checks if a `String` is a valid regular expression
* `StartsWith[S]`: checks if a `String` starts with the prefix `S`
* `Uri`: checks if a `String` is a valid URI
* `Url`: checks if a `String` is a valid URL
* `Uuid`: checks if a `String` is a valid UUID
* `ValidByte`: checks if a `String` is a parsable `Byte`
* `ValidShort`: checks if a `String` is a parsable `Short`
* `ValidInt`: checks if a `String` is a parsable `Int`
* `ValidLong`: checks if a `String` is a parsable `Long`
* `ValidFloat`: checks if a `String` is a parsable `Float`
* `ValidDouble`: checks if a `String` is a parsable `Double`
* `ValidBigInt`: checks if a `String` is a parsable `BigInt`
* `ValidBigDecimal`: checks if a `String` is a parsable `BigDecimal`
* `Xml`: checks if a `String` is well-formed XML
* `XPath`: checks if a `String` is a valid XPath expression
* `Trimmed`: checks if a `String` has no leading or trailing whitespace
* `HexStringSpec`: checks if a `String` represents a hexadecimal number
