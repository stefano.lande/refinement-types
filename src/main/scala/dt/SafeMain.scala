package dt

import dt.SafeMain.Splitter.split

object SafeMain extends App {

  val list      = List(1, 2, 3, 4, 5, 6, 7)
  val splitList = split(list, chunkSize = 2)
  println(splitList)

  val list2      = List(1, 2, 3, 4, 5, 6, 7)
  val splitList2 = split(list, chunkSize = -1)
  println(splitList2)

  object Splitter {

    sealed trait SplittingError
    case object NegativeChunkSize extends SplittingError

    def split[A](list: List[A], chunkSize: Int): Either[SplittingError, List[List[A]]] = {

      def innerSplit[B](xs: List[B], n: Int): List[List[B]] =
        if (xs.isEmpty) Nil
        else (xs take n) :: innerSplit(xs drop n, n)

      chunkSize match {
        case _ if chunkSize > 0 => Right(innerSplit(list, chunkSize))
        case _                  => Left(NegativeChunkSize)
      }
    }
  }

}
