package dt

import dt.TypedMain.Splitter.split
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.numeric.Positive

object TypedMain extends App {

  val list      = List(1, 2, 3, 4, 5, 6, 7)
  val splitList = split(list, chunkSize = 2)
  println(splitList)

  val list2      = List(1, 2, 3, 4, 5, 6, 7)
  val splitList2 = split(list2, chunkSize = 1)
  println(splitList2)

  object Splitter {

    def split[A](list: List[A], chunkSize: Int Refined Positive): List[List[A]] =
      if (list.isEmpty) Nil
      else (list take chunkSize) :: split(list drop chunkSize, chunkSize)
  }

}
