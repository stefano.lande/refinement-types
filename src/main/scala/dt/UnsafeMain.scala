package dt

import dt.UnsafeMain.Splitter.split

object UnsafeMain extends App {

  val list      = List(1, 2, 3, 4, 5, 6, 7)
  val splitList = split(list, chunkSize = 2)
  println(splitList)

  val list2      = List(1, 2, 3, 4, 5, 6, 7)
  val splitList2 = split(list2, chunkSize = 0)
  println(splitList2)

  object Splitter {

    def split[A](list: List[A], chunkSize: Int): List[List[A]] =
      if (list.isEmpty) Nil
      else (list take chunkSize) :: split(list drop chunkSize, chunkSize)
  }

}
